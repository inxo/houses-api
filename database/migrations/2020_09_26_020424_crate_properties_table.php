<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CratePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $t) {
            $t->increments('id');
            $t->string('name');
            $t->unsignedInteger('price');
            $t->unsignedInteger('bedrooms');
            $t->unsignedInteger('bathrooms');
            $t->unsignedInteger('storeys');
            $t->unsignedInteger('garages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('properties');
    }
}
