<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use SplFileObject;

class PropertySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = new SplFileObject(__DIR__.'/property-data.csv','r');

        while($item = $csv->fgetcsv()) {
            $data[] = $item;
        }
        $keys = array_map(function ($elm){
            return strtolower($elm);
        },array_shift($data));
        foreach ($data as $i => $row) {
            $data[$i] = array_combine($keys, $row);
        }
        DB::table('properties')->insert($data);
    }
}
