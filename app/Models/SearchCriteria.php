<?php


namespace App\Models;


class SearchCriteria
{
    private $name;
    private $bedrooms;
    private $bathrooms;
    private $storeys;
    private $garages;
    private $price_from;
    private $price_to;

    public function __construct(array $criteria) {
        foreach ($criteria as $key => $value) {
            if(property_exists($this,$key)) {
                $this->{$key} = $value;
            }
        }
    }

    public function getName(): ?string
    {
        return (!empty($this->name)) ? $this->name : null;
    }

    public function getBedrooms(): ?int
    {
        return $this->bedrooms;
    }

    public function getBathrooms(): ?int
    {
        return $this->bathrooms;
    }

    public function getStoreys(): ?int
    {
        return $this->storeys;
    }

    public function getGarages(): ?int
    {
        return $this->garages;
    }

    public function getPriceFrom(): ?int
    {
        return (!empty($this->price_from)) ? $this->price_from : null;
    }

    public function getPriceTo(): ?int
    {
        return (!empty($this->price_to)) ? $this->price_to : null;
    }


}
