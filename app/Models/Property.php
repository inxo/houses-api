<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use JsonSerializable;

/**
 * Class Property
 * @package App\Models
 *
 * @property string $name
 * @property int $bedrooms
 * @property int $bathrooms
 * @property int $storeys
 * @property int $garages
 * @property int $price
 */
class Property extends Model implements JsonSerializable
{
    use HasFactory;

}
