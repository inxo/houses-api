<?php


namespace App\Exceptions;

use Exception;
use Throwable;

class ApiException extends Exception
{
    private $errors;

    public function __construct($message = "", $code = 0, Throwable $previous = null, $errors = [])
    {
        $this->errors = $errors;
        parent::__construct($message, $code, $previous);
    }

}
