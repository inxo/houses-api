<?php

namespace App\Http\Requests;

use App\Exceptions\ApiException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class SearchCriteria extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|nullable',
            'bedrooms' => 'integer|min:0',
            'bathrooms' => 'integer|min:0',
            'garages' => 'integer|min:0',
            'storeys' => 'integer|min:0',
            'price_from' => 'integer|nullable|min:0',
            'price_to' => 'integer|nullable|min:0'
        ];
    }


    /**
     * @param Validator $validator
     * @throws ApiException
     */
    public function failedValidation(Validator $validator) {
        throw new ApiException('Wrong request', 400, null, $validator->errors());
    }
}
