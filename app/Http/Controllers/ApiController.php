<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchCriteria;
use App\Models\SearchCriteria as Filter;
use App\Repositories\PropertyRepository;
use Illuminate\Http\JsonResponse;

class ApiController extends Controller
{
    public function index(SearchCriteria $searchCriteria): JsonResponse
    {
        //sleep(5); // test loader
        $propertyRepository = new PropertyRepository();
        $validated = $searchCriteria->validated();
        $results = $propertyRepository->findByCriteria(new Filter($validated));
        return new JsonResponse($results->toJson());
    }
}
