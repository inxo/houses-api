<?php


namespace App\Repositories;


use App\Models\Property;
use App\Models\SearchCriteria;
use Illuminate\Database\Eloquent\Collection;

class PropertyRepository
{
    /**
     * @param SearchCriteria $searchCriteria
     * @return Collection
     */
    public function findByCriteria(SearchCriteria $searchCriteria): Collection
    {
        $query = Property::query();

        if($searchCriteria->getName()) {
            $query->where('name','LIKE','%'.$searchCriteria->getName().'%');
        }

        if(is_int($searchCriteria->getBedrooms())) {
            $query->where('bedrooms',$searchCriteria->getBedrooms());
        }

        if(is_int($searchCriteria->getBathrooms())) {
            $query->where('bathrooms',$searchCriteria->getBathrooms());
        }

        if(is_int($searchCriteria->getGarages())) {
            $query->where('garages',$searchCriteria->getGarages());
        }

        if(is_int($searchCriteria->getStoreys())) {
            $query->where('storeys',$searchCriteria->getStoreys());
        }

        // for condition: Range (between $X and $Y)
        if($searchCriteria->getPriceFrom() && $searchCriteria->getPriceTo()) {
            $query->whereBetween('price', [$searchCriteria->getPriceFrom(),$searchCriteria->getPriceTo()]);
        }

        return $query->get();
    }
}
